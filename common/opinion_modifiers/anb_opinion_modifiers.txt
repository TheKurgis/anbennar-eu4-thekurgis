#########################################################################
# OPINION MODIFIERS
##########################################################################

##########################################################################
# HARD - DO NOT REMOVE OR RENAME ANY OF THESE
##########################################################################

##############
# FIXED STATE
##############

opinion_magocracy_noble = {
	opinion = 50
	years = 50
	yearly_decay = 1
}

opinion_adventurer_noble = {
	opinion = 50
	years = 50
	yearly_decay = 1
}

root_evil = {
	opinion = -50
	min_opinion = -50
}

both_evil = {
	opinion = 5
	max_opinion = 5
}

expelled_the_magisterium = {
	opinion = -200
}

welcomed_the_magisterium = {
	opinion = 50
}

magisterium_disagrees_with_direction = {
	opinion = -50
}

opinion_countytoduchy_accept = {
	opinion = 100
	years = 20
	yearly_decay = 5
}

opinion_countytoduchy_reject = {
	opinion = -100
	years = 20
	yearly_decay = 5
}

root_monstrous = {
	opinion = -100
	min_opinion = -100
}

both_monstrous = {
	opinion = 20
	max_opinion = 20
}

not_monstrous = {
	opinion = 25
}

lilac_wars_moon_party_member = {	#Moon Party Member - Fought alongside us during the Lilac Wars
	opinion = 40
	years = 20
	yearly_decay = 2
}

lilac_wars_moon_party_enemy = {	#Rose Party Member - Defeated us in the Lilac Wars
	opinion = -40
	years = 20
	yearly_decay = 2
}

lilac_wars_rose_party_member = {	#Rose Party Member - Won the Lilac Wars together
	opinion = 20
	years = 20
	yearly_decay = 1
}

lilac_wars_rose_party_enemy = {	#Moon Party Member - Defeated them during the Lilac Wars
	opinion = -40
	years = 40
	yearly_decay = 1
}

lorent_claimed_wine_lords = {
	opinion = -75
	years = 75
	yearly_decay = 1
}

lorent_did_not_claim_wine_lords = {
	opinion = 60
	years = 10
	yearly_decay = 6
}

beepeck_secession = {
	opinion = -100
	years = 20
	yearly_decay = 5
}

A04_laurens_chose_high_lorentish = {
	opinion = -20
	years = 20
	yearly_decay = 1
}

A04_laurens_chose_west_damerian = {
	opinion = -20
	years = 20
	yearly_decay = 1
}

A48_support_during_succession_war = {
	opinion = 50
	years = 50
	yearly_decay = 1
}

A48_supported_rebels = {
	opinion = -50
	years = 50
	yearly_decay = 1
}

A48_didnt_support_rebels = {
	opinion = 20
	years = 40
	yearly_decay = 0.5
}

A48_gave_important_province = {
	opinion = 50
	years = 50
	yearly_decay = 1
}

A48_didnt_paid_us_back = {
	opinion = -50
	years = 50
	yearly_decay = 1
}

A13_burned_the_redglades_anb = {
	opinion = -100
	years = 50
	yearly_decay = 2
}

A13_burned_the_redglades_elf = {
	opinion = -200
	years = 400
	yearly_decay = 1
}

A38_sacked_anbenncost = {
	opinion = -200
	years = 100
	yearly_decay = 2
}

destruction_of_temples = {	#Triggered by goblinic_shamanism_flavor.58
	opinion = -50
	years = 50
	yearly_decay = 1
}

party_insult_mild = {
	opinion = -50
	years = 10
	yearly_decay = 5
}

party_insult_severe = {
	opinion = -100
	years = 20
	yearly_decay = 5
}

party_compliment_mild = {
	opinion = 50
	years = 10
	yearly_decay = 5
}

party_compliment_huge = {
	opinion = 100
	years = 20
	yearly_decay = 5
}