country_decisions = {

	black_demense_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_black_demense_flag }
			NOT = { tag = Z01 } #Empire of Anbennar
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				NOT = { exists = Z99 }
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
			government = theocracy
			capital_scope = { continent = europe }
			
			has_country_flag = lich_ruler
			
			has_country_flag = disabled	#this is just a debuggy thing so prevent people from getting it
		}
		allow = {
			is_at_war = no
			is_free_or_tributary_trigger = yes
			is_nomad = no			
			NOT = { exists = Z99 }
			
			num_of_cities = 100
			
			has_country_flag = lich_ruler
			
		}
		effect = {
			change_tag = Z99
			swap_non_generic_missions = yes
			#remove_non_electors_emperors_from_empire_effect = yes
			if = {
				limit = {
					NOT = { government_rank = 3 }
				}
				set_government_rank = 3
			}
			
			if = {
				limit = {
					NOT = { government = theocracy }
				}
				change_government = theocracy
				add_government_reform = magocracy_reform	#make it unique later
			}
			
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			add_prestige = 50
			if = {
				limit = {
					has_custom_ideas = no
					OR = {
						ai = no
						AND = {
							ai = yes
							has_idea_group = default_ideas
						}
					}
				}
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			set_country_flag = formed_black_demense_flag
		}
		ai_will_do = {
			factor = 1
		}
	}
	
}
