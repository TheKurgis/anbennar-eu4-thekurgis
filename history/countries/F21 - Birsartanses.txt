government = monarchy
add_government_reform = autocracy_reform
government_rank = 2
primary_culture = sun_elf
add_accepted_culture = zanite
add_accepted_culture = gelkar
add_accepted_culture = brasanni
add_accepted_culture = bahari
religion = bulwari_sun_cult
technology_group = tech_bulwari
capital = 549
historical_rival = F22 #Dartaxagerdim
historical_friend = F26 #historical subject

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Arantir I"
		dynasty = "Birsartanszuir"
		birth_date = 1391.7.3
		adm = 3
		dip = 4
		mil = 3
}
	heir = { 
		name = "Birsartan"
		monarch_name = "Birsartan III"
		dynasty = "Birsartanszuir"
		birth_date = 1417.12.12
		death_date = 1525.1.1
		claim = 80
		adm = 2
		dip = 2
		mil = 4
	}
	add_ruler_personality = lawgiver_personality
}