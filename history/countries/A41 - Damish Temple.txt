government = theocracy
add_government_reform = leading_clergy_reform
government_rank = 1
primary_culture = east_damerian
religion = regent_court
technology_group = tech_cannorian
capital = 4

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }


1404.1.1 = {
	monarch = {
		name = "Calas s�na Arb"
		birth_date = 1380.1.7
		adm = 3
		dip = 0
		mil = 1
	}
}