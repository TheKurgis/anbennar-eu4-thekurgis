# No previous file for Taraz
owner = F10
controller = F10
add_core = F10
culture = sandfang_gnoll
religion = xhazobkult

hre = no

base_tax = 2
base_production = 1
base_manpower = 2

trade_goods = slaves

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish