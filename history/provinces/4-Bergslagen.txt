#Bergslagen, contains Falun, V�ster�s etc.

owner = A41
controller = A41
add_core = A41
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 5
base_production = 5
base_manpower = 5

trade_goods = wool

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
discovered_by = tech_gnollish