#434 - Quhistan

owner = A59
controller = A59
add_core = A59
culture = corvurian
religion = regent_court

hre = no

base_tax = 3
base_production = 3
base_manpower = 4

trade_goods = livestock

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish



