# No previous file for Konungrhavn
owner = Z08
controller = Z08
add_core = Z08
culture = east_dalr
religion = skaldhyrric_faith

hre = no

base_tax = 5
base_production = 5
base_manpower = 3

trade_goods = grain
center_of_trade = 1

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind
