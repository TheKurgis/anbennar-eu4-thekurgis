#333 - The Baleares

owner = A91
controller = A91
add_core = A91
culture = esmari
religion = regent_court

hre = yes

base_tax = 6
base_production = 4
base_manpower = 4

trade_goods = grain

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
