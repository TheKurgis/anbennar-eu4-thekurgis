#Akershus, incl. Oslo (Christiania), T�nsberg
 
owner = A04
controller = A04
add_core = A04
culture = west_damerian
religion = regent_court
hre = yes
base_tax = 4
base_production = 4
trade_goods = iron
base_manpower = 4
capital = ""
is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
