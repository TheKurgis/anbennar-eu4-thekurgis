#903 - Omaha

owner = Z04
controller = Z04
add_core = Z04
culture = wexonard
religion = regent_court

hre = yes

base_tax = 5
base_production = 4
base_manpower = 3

trade_goods = livestock

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish