#235 - Kent |

owner = A13
controller = A13
add_core = A13
culture = vertesker
religion = regent_court

hre = yes

base_tax = 3
base_production = 4
base_manpower = 3

trade_goods = grain

capital = ""

is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
