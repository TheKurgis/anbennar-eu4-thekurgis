#162 - Zemplen | Aelcandar

owner = A01
controller = A01
add_core = A01
culture = redfoot_halfling
religion = regent_court

hre = no

base_tax = 2
base_production = 3
base_manpower = 6

trade_goods = grain

capital = "Calasandur's West Castle"

is_city = yes
fort_15th = yes 


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold

add_permanent_province_modifier = {
	name = calasandurs_elven_castle
	duration = -1
}