#799

owner = B41
controller = B41
add_core = B41
culture = common_goblin
religion = goblinic_shamanism


base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = livestock

capital = ""

is_city = yes

native_size = 54
native_ferocity = 5
native_hostileness = 10

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari